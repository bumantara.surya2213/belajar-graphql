const express = require("express");
const mysql = require("mysql");
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'media'
})
connection.connect();
let post = [];
let user = [];
connection.query('SELECT * from user', (err, rows, fields) => {
    if (err) throw err
    
    rows.forEach(element => {
        
        user.push({
            id: element.id,
            username: element.username,
            email: element.email,
            password: element.password,
        })
        console.log(user.length)
    });
})
connection.query('SELECT * from post', (err, rows, fields) => {
    if (err) throw err
    
    rows.forEach(element => {
        post.push({
            id: element.id,
            title: element.title,
            content: element.content,
            user_id: element.user_id,
        })
    });
})

const { graphqlHTTP }  = require("express-graphql");
const { GraphQLSchema, GraphQLObjectType, GraphQLNonNull, GraphQLInt, GraphQLString, GraphQLList } = require("graphql");
const app = express();

const UserType = new GraphQLObjectType({
    name: "users",
    description: "this is the users registered",
    fields: () => ({
        id: { type: GraphQLNonNull(GraphQLInt) },
        username: { type: GraphQLNonNull(GraphQLString) },
        email: { type: GraphQLNonNull(GraphQLString) },
        password: { type: GraphQLNonNull(GraphQLString) },
        posts: {
            type: new GraphQLList(PostType),
            resolve: (user) => {
                return post.filter(post => user.id === post.user_id)
            }
        }
    })
})

const PostType = new GraphQLObjectType({
    name: "posts",
    description: "this is the post created by users",
    fields: () => ({
        id: { type: GraphQLNonNull(GraphQLInt) },
        title: { type: GraphQLNonNull(GraphQLString) },
        content: { type: GraphQLNonNull(GraphQLString) },
        user_id: { type: GraphQLNonNull(GraphQLString) },
        user: {
            type: UserType,
            resolve: (post) => {
                return user.find(user => user.id === post.user_id)
            }
        }
    })
})

const myQuery = new GraphQLObjectType({
    name: "query",
    description: 'Root Query',
    fields: () => ({
        users: {
            type: new GraphQLList(UserType),
            description: 'List of all Users',
            resolve: () => user
        },
        user: {
            type: UserType,
            description: 'A User',
            args: {
                id: { type: GraphQLInt }
            },
            resolve: (parent, args) => user.find(user => user.id === args.id)
            
        },
        posts: {
            type: new GraphQLList(PostType),
            description: 'List of all Posts',
            resolve: () => post
        },
        post: {
            type: PostType,
            description: 'A Post',
            args: {
                id: { type: GraphQLInt }
            },
            resolve: (parent, args) => post.find(post => post.id === args.id)
        },
    })
})

const myMutation = new GraphQLObjectType({
    name: "mutation",
    description: "Root Mutation",
    fields: () => ({
        addUser: {
            type: UserType,
            description: "add a new User",
            args:{
                username: { type: GraphQLNonNull(GraphQLString) },
                email: { type: GraphQLNonNull(GraphQLString) },
                password: { type: GraphQLNonNull(GraphQLString) },
            },
            resolve: (parent, args) => {
                const user = {
                    username: args.username,
                    email: args.email,
                    password: args.email,
                };
                user.push(user);
                return user
            }
        }
    })
})

const mySchema = new GraphQLSchema({
    query: myQuery,
    mutation: myMutation
})

app.use("/graphql", graphqlHTTP({
    schema: mySchema,
    graphiql: true
}))
app.listen(5000, () => console.log("Server is Running"))